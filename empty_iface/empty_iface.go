package main

import "fmt"

func main() {
	// var i interface{}  // go < 1.18
	var i any

	i = 7
	fmt.Println(i)

	// i += 1 // won't compile

	i = "Hi"
	fmt.Println(i)

	s := i.(string) // type assertion
	fmt.Println("s:", s)

	// n := i.(int) // panic
	n, ok := i.(int)
	if ok {
		fmt.Println("n:", n)
	} else {
		fmt.Println("not an int")
	}
}
