package main

import (
	"fmt"
	"log"
)

func main() {
	var i Item
	fmt.Println("i:", i)
	fmt.Printf("v: %v\n", i)
	fmt.Printf("+v: %+v\n", i)
	fmt.Printf("#v: %#v\n", i)

	i = Item{1, 2} // must provide all fields in order
	fmt.Printf("i: %#v\n", i)

	i = Item{Y: 10} //, X: 20} // by name, order not required, can omit fields
	fmt.Printf("i: %#v\n", i)
	i.X = 9
	fmt.Printf("i: %#v\n", i)

	fmt.Println(NewItem(100, 200))
	fmt.Println(NewItem(100, 2000))

	i2, err := NewItem(50, 60)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	// C++: i2->X
	fmt.Println("i2.X", i2.X)

	i.Move(15, 17)
	fmt.Printf("i (move): %#v\n", i)

	p1 := Player{
		Name: "Parzival",
		Item: Item{500, 700},
	}
	fmt.Printf("p1: %#v\n", p1)
	fmt.Printf("p1.X: %#v\n", p1.X)
	p1.Move(9, 12)
	fmt.Printf("p1 (move): %#v\n", p1)

	ms := []Mover{
		&i,
		i2,
		&p1,
	}
	moveAll(ms, 0, 0)
	for _, m := range ms {
		fmt.Println(m)
	}

	/*
		fmt.Println(p1.Found("gold"))
		p1.Found("copper")
		p1.Found("copper")
	*/
	fmt.Println(p1.Found(Key(17)))
	p1.Found(Copper)
	p1.Found(Copper)

	fmt.Println(p1.Keys)

	fmt.Println(Copper)
	fmt.Printf("s: %s\n", Copper)
	fmt.Printf("v: %v\n", Copper)
	fmt.Printf("+v: %+v\n", Copper)
	fmt.Printf("#v: %#v\n", Copper)
}

/* What fmt.Printf and friends does
func Print(v any) {
	if f, ok := v.(Stringer) {
		print(f.String())
		return
	}

	switch v.(type) {
	case int:
		...
	}
}
*/

// implement fmt.Stringer interface
func (k Key) String() string {
	switch k {
	case Copper:
		return "copper"
	case Jade:
		return "jade"
	case Crystal:
		return "crystal"
	}

	return fmt.Sprintf("<Key %d>", k)
}

type Key byte

const (
	Copper Key = iota + 1
	Jade
	Crystal
)

func (p *Player) Found(key Key) error {
	if !key.Valid() {
		return fmt.Errorf("unknown key: %q", key)
	}

	if !p.hasKey(key) {
		p.Keys = append(p.Keys, key)
	}
	return nil
}

func (p *Player) hasKey(key Key) bool {
	for _, k := range p.Keys {
		if key == k {
			return true
		}
	}
	return false
}

func (k Key) Valid() bool {
	switch k {
	case Jade, Copper, Crystal:
		return true
	}
	return false
}

func moveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}

/*
- Add a Keys []string field to Player
- Add a Found(key string) error method to Player
	- key should be one of "copper", "jade", "crystal"
	- a key should be added only once
		p1.Found("copper")
		p1.Found("copper")
		fmt.Println(p1.Keys) // [copper]
*/

/*
Go:
type Reader interface {
	Read([]byte) (int, error)
}

Python:
type Reader interface {
	Read(int) ([]byte, error)
}

func Sort(s Sortable) {
	...
}

type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}
*/

// interfaces specify what we need, not what we provide => Go interfaces are small
// rule of thumb: accept interfaces, return types
type Mover interface {
	Move(int, int)
}

type Player struct {
	Name string
	Item // Player embeds Item
	Keys []Key
}

// i is called "the receiver"
// Use a pointer receiver when changing the struct
func (i *Item) Move(x, y int) {
	i.X = x
	i.Y = y
}

// func NewItem(x, y int) Item
// func NewItem(x, y int) *Item
// func NewItem(x, y int) (Item, error)
func NewItem(x, y int) (*Item, error) {
	if x < 0 || x > maxX || y < 0 || y > maxY {
		return nil, fmt.Errorf("%d/%d out of bounds for %d/%d", x, y, maxX, maxY)
		// return Item{}, fmt.Errorf("%d/%d out of bounds for %d/%d", x, y, maxX, maxY)
	}

	i := Item{
		X: x,
		Y: y,
	}
	// the Go compiler does escape analysis and will allocate i on the heap
	return &i, nil
}

const (
	maxX = 1000
	maxY = 600
)

type Item struct {
	X int
	Y int
}
