package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	/* Fix 1: mutex
	var mu sync.Mutex
	counter := 0
	*/
	var counter int64

	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for j := 0; j < 1000; j++ {
				time.Sleep(time.Microsecond)
				/* Fix 1: Use a mutex
				mu.Lock()
				counter++
				mu.Unlock()
				*/
				atomic.AddInt64(&counter, 1)
			}
		}()
	}
	wg.Wait()

	fmt.Println(counter)
}
