package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

/* What is the most common word (ignoring case) in sherlock.txt
- regular expressions ✓
- reading one line at a time ✓
- maps
*/

var (
	// "Who's on first?" -> [Who s on first]
	wordRe = regexp.MustCompile(`[A-Za-z]+`)
)

func main() {
	file, err := os.Open("sherlock.txt")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	defer file.Close()

	s := bufio.NewScanner(file)
	freq := make(map[string]int) // word -> count
	for s.Scan() {
		for _, word := range wordRe.FindAllString(s.Text(), -1) {
			word = strings.ToLower(word)
			freq[word]++
		}
	}

	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}

	maxW, maxC := "", 0
	for w, c := range freq {
		if c > maxC {
			maxW, maxC = w, c
		}
	}
	fmt.Println(maxW)

	/*
		text := "Who's on first?"
		fmt.Println(wordRe.FindAllString(text, -1))
	*/

	// scanDemo()
	// mapDemo()

}

func mapDemo() {
	var stocks map[string]float64
	fmt.Println(stocks, len(stocks))
	fmt.Println(stocks["AAPL"]) // 0
	v, ok := stocks["AAPL"]
	fmt.Println(v, ok)
	if _, ok := stocks["AAPL"]; !ok {
		fmt.Println("no AAPL")
	}

	stocks = make(map[string]float64)
	stocks["AAPL"] = 148.59     // will panic on nil map
	fmt.Println(stocks["AAPL"]) // 0

	stocks = map[string]float64{
		"AAPL": 148.59,
		"WIX":  91.82,
	}

	for k := range stocks { // keys
		fmt.Println(k)
	}

	for k, v := range stocks { // keys + values
		fmt.Println(k, "->", v)
	}
}

func scanDemo() {
	poem := `
Monday's child is fair of face,
Tuesday's child is full of grace.
Wednesday's child is full of woe,
Thursday's child has far to go.
Friday's child is loving and giving,
Saturday's child works hard for a living.
And the child born on the Sabbath day
Is bonny and blithe, good and gay.
	`

	r := strings.NewReader(poem)
	s := bufio.NewScanner(r)
	n := 0
	for s.Scan() {
		n++
		fmt.Println(n, s.Text())
	}
	if err := s.Err(); err != nil {
		log.Fatalf("error: %s", err)
	}
}

func init() {
	// runs before main.main
}
