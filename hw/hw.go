/* A simple hello world */
// one line comment
package main

// import "fmt"
import (
	"fmt"
)

func main() {
	fmt.Println("Hello Gophers ♡")
}
