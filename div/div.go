package main

import (
	"fmt"
)

func main() {
	fmt.Println(safeDiv(7, 3))
	fmt.Println(safeDiv(7, 0))
	fmt.Println("fin")
}

// named return values - mostly used with recover, also good for documentation
func safeDiv(a, b int) (q int, err error) {
	// q and err are local variables in safeDiv (just like a & b)
	defer func() {
		if e := recover(); e != nil {
			//	fmt.Println("ERROR:", e)
			err = fmt.Errorf("%s", e) // convert e (any) to error
		}
	}()

	/* Don't do this :)
	q, err = div(a, b), nil
	return
	*/
	return div(a, b), nil
}

func div(a, b int) int {
	return a / b
}
