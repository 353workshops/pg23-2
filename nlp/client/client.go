package client

import (
	"fmt"
	"net/http"
)

type Client struct {
	baseURL string
	c       http.Client
}

func (c *Client) Health() error {
	url := c.baseURL + "/health"
	resp, err := c.c.Get(url)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%q: bad status: %s", url, resp.Status)
	}

	return nil
}
