package client

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

type errTripper struct{}

// Implement http.RoundTripper (mock)
func (e errTripper) RoundTrip(*http.Request) (*http.Response, error) {
	return nil, fmt.Errorf("connection error")
}

func TestHealthError(t *testing.T) {
	c := Client{
		baseURL: "https://example.com",
	}
	c.c.Transport = errTripper{} // mocking

	err := c.Health()
	require.Error(t, err)
}
