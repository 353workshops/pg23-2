package stemmer_test

import (
	"fmt"

	"github.com/ardanlabs/nlp/stemmer"
)

func ExampleStem() {
	fmt.Println(stemmer.Stem("working"))
	fmt.Println(stemmer.Stem("worked"))
	fmt.Println(stemmer.Stem("works"))

	// Output:
	// work
	// work
	// work
}
