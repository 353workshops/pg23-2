package nlp

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

/* Exercise: Read tests cases from
testdata/tokenize_cases.yml

And use them instead of in memory testCases slice

For YAML use gopkg.in/yaml.v3
*/

type tokenizeCase struct {
	Name   string
	Text   string
	Tokens []string
}

func loadTokenizeCases(t *testing.T) []tokenizeCase {
	file, err := os.Open("testdata/tokenize_cases.yml")
	require.NoError(t, err)
	defer file.Close()

	var tc []tokenizeCase
	err = yaml.NewDecoder(file).Decode(&tc)
	require.NoError(t, err, "parse YAML")
	return tc
}

func TestTokenizeTable(t *testing.T) {
	/*
		var testCases = []struct {
			name   string
			text   string
			tokens []string
		}{
			{"empty", "", nil},
			{"simple", "What's on second?", []string{"what", "s", "on", "second"}},
		}
	*/

	for _, tc := range loadTokenizeCases(t) {
		name := tc.Name
		if name == "" {
			name = tc.Text
		}
		t.Run(name, func(t *testing.T) {
			tokens := Tokenize(tc.Text)
			require.Equal(t, tc.Tokens, tokens)
		})
	}
}

func TestTokenize(t *testing.T) {
	// fixtures
	// setup()
	// defer teardown()

	text := "Who's on first?"
	tokens := Tokenize(text)
	expected := []string{"who", "on", "first"}
	require.Equal(t, expected, tokens)
	/* before testify
	if !reflect.DeepEqual(expected, tokens) {
		t.Fatalf("expected: %v, got: %v", expected, tokens)
	}
	*/
}

func inCI() bool {
	// On Jenkins use BUILD_NUMBER
	return os.Getenv("CI") != ""
}

func TestInCI(t *testing.T) {
	if !inCI() {
		t.Skip("Not in CI")
	}
	t.Log("In CI")
}

func FuzzTokenize(f *testing.F) {
	f.Add("") // Example to check
	fn := func(t *testing.T, text string) {
		tokens := Tokenize(text)
		lText := strings.ToLower(text)
		for _, tok := range tokens {
			if !strings.Contains(lText, tok) {
				t.Fatal(tok)
			}
		}
	}
	f.Fuzz(fn)
}
