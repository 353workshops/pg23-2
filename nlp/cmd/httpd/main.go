package main

import (
	"encoding/json"
	"expvar"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"github.com/ardanlabs/nlp"
	"github.com/ardanlabs/nlp/stemmer"
)

// config: default, config file, environment, command line
// default - in code
// config file: YAML, TOML, viper
// environment: os.Getenv, github.com/ardanlabs/conf, ...
// command line: flag, cobra

func main() {
	logger := zap.NewExample().Sugar()
	srv := API{
		log:           logger,
		tokenizeCalls: expvar.NewInt("tokenize.calls"),
	}
	// routing rules:
	// if ends with "/" - prefix match
	// otherwise - exact match
	r := chi.NewRouter()
	/* Before chi
	http.HandleFunc("/health", healthHandler)
	http.HandleFunc("/tokenize", tokenizeHandler)
	*/
	r.Get("/health", srv.healthHandler)
	r.Post("/tokenize", srv.tokenizeHandler)
	r.Get("/stem/{word}", srv.stemHandler)
	// http.Handle("/", r)
	r.Handle("/debug/vars", expvar.Handler())

	addr := os.Getenv("ADDR")
	if addr == "" {
		addr = ":8080"
	}
	logger.Infow("starting", "addr", addr)

	if err := http.ListenAndServe(addr, r); err != nil {
		log.Fatalf("error: %s", err)
	}
}

type API struct {
	tokenizeCalls *expvar.Int
	log           *zap.SugaredLogger
}

// $ curl http://localhost:8080/stem/works
// work
func (a *API) stemHandler(w http.ResponseWriter, r *http.Request) {
	word := chi.URLParam(r, "word")
	if word == "" {
		a.log.Errorw("empty word", "path", r.URL.Path)
		http.Error(w, "empty work", http.StatusBadRequest)
		return
	}

	stem := stemmer.Stem(word)
	fmt.Fprintln(w, stem)
}

/*
	Exercise:

Write tokenizeHandler which will get text in request body to /tokenize
and return JSON with tokens

$ curl -d"Who's on first?" http://localhost:8080/tokenize
{"tokens": ["who", "on", "first"]}
*/
func (a *API) tokenizeHandler(w http.ResponseWriter, r *http.Request) {
	/* before chi
	if r.Method != http.MethodPost {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/

	a.tokenizeCalls.Add(1)
	rdr := io.LimitReader(r.Body, 1_000_000)
	data, err := io.ReadAll(rdr)
	if err != nil {
		a.log.Errorw("can't read", "error", err)
		http.Error(w, "can't read", http.StatusBadRequest)
		return

	}

	text := string(data)
	tokens := nlp.Tokenize(text)

	resp := map[string]any{
		"tokens": tokens,
	}

	if err := sendJSON(w, resp); err != nil {
		log.Printf("ERROR: can't write response - %s", err)
	}
}

func sendJSON(w http.ResponseWriter, v any) error {
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(v)
}

func (a *API) healthHandler(w http.ResponseWriter, r *http.Request) {
	/* before chi
	if r.Method != http.MethodGet {
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	*/

	// TODO: Real health check
	fmt.Fprintln(w, "OK")
}
