package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	go fmt.Println("goroutine")
	fmt.Println("main")

	for i := 0; i < 3; i++ {
		/* Fix 2: Use loop variable */
		i := i
		go func() {
			fmt.Println("for:", i) // i from line 14
		}()
		/* Fix 1: Use a parameter
		go func(n int) {
			fmt.Println("for:", n)
		}(i)
		*/
		/* BUG: All goroutines use the same i from line 12
		go func() {
			fmt.Println("for:", i)
		}()
		*/
	}

	time.Sleep(time.Millisecond)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	fmt.Println(sleepSort([]int{20, 10, 15})) // [10 15 20]

	go func() {
		for i := 0; i < 4; i++ {
			ch <- i
		}
		close(ch)
	}()

	for val := range ch {
		fmt.Println("range:", val)
	}
	/* for range does:
	for {
		val, ok := <- ch
		if !ok {
			break
		}
		fmt.Println("range:", val)
	}
	*/

	val = <-ch // ch is closed
	fmt.Println("closed:", val)

	val, ok := <-ch // ch is closed
	fmt.Println("closed:", val, ok)
	// ch <- 7 // panic

	queue := make(chan string)
	for i := 0; i < 3; i++ {
		go producer(i, 3, queue)
		go consumer(i, queue)
	}

	time.Sleep(100 * time.Millisecond)

	ch1, ch2 := make(chan string), make(chan string)

	go func() {
		time.Sleep(10 * time.Millisecond)
		ch1 <- "one"
	}()

	go func() {
		time.Sleep(20 * time.Millisecond)
		ch2 <- "two"
	}()

	/*
		done := make(chan bool)
		go func() {
			time.Sleep(time.Millisecond)
			close(done)
		}()
	*/

	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()

	select {
	case val := <-ch1:
		fmt.Println("ch1:", val)
	case val := <-ch2:
		fmt.Println("ch2:", val)
	// case <-done:
	// case <-time.After(time.Millisecond):
	case <-ctx.Done():
		fmt.Println("timeout")
	}
}

func producer(id, count int, ch chan<- string) {
	for i := 0; i < count; i++ {
		msg := fmt.Sprintf("%d -> [%d]", id, i)
		ch <- msg
		randSleep() // simulate work
	}
}

func consumer(id int, ch <-chan string) {
	for msg := range ch {
		fmt.Printf("%s -> %d\n", msg, id)
		randSleep() // simulate work
	}
}

/* Example with done channel
func consumer(id int, ch <-chan string, done <-chan bool) {
	for {
		select {
		case msg, ok := <-ch:
			if !ok {
				return
			}
			fmt.Printf("%s -> %d\n", msg, id)
			randSleep() // simulate work
		case <-done:
			return
		}
	}
}
*/

func randSleep() {
	n := rand.Intn(100)
	time.Sleep(time.Duration(n) * time.Millisecond)

}

/*
	Algorithm:

For every number "n" in values, spin a goroutine that will
- sleep n milliseconds
- send n over a channel

Collect values from the channel to a slice and return it.
*/
func sleepSort(values []int) []int {
	ch := make(chan int)

	// fan out
	for _, n := range values {
		go func(i int) {
			time.Sleep(time.Duration(i) * time.Millisecond)
			ch <- i
		}(n)
	}

	var sorted []int
	for range values {
		n := <-ch
		sorted = append(sorted, n)
	}

	return sorted
}

/* Channel semantics
- send/receive on a channel will block until opposite operation (*)
	- buffered channel of capacity "n" has "n" non-blocking sends
- receive from a closed channel will return zero value without blocking
- send to a closed channel will panic
- closing a closed channel will panic
- send/receive to/from a nil channel will block forever
*/
