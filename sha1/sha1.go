package main

import (
	"compress/gzip"
	"crypto/sha1"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	sig, err := fileSHA1("http.log.gz")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(sig)

	sig, err = fileSHA1("sha1.go")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(sig)
}

// $ cat http.log.gz | gunzip |  sha1sum
// exercise: If the file does not end with .gz, don't decompress it
// $ cat sha1.go | sha1sum
// Hint: strings.HasSuffix
func fileSHA1(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file

	if strings.HasSuffix(fileName, ".gz") {
		// r, err := gzip.NewReader(file) // BUG
		// var err error
		r, err = gzip.NewReader(file)
		if err != nil {
			return "", fmt.Errorf("%q: not gzip - %s", fileName, err)
		}
	}

	// Show 1K of decompressed content
	// io.CopyN(os.Stdout, r, 1024)

	w := sha1.New()
	if _, err := io.Copy(w, r); err != nil {
		return "", fmt.Errorf("%q: can't copy - %s", fileName, err)
	}

	data := w.Sum(nil)
	return fmt.Sprintf("%x", data), nil
}
