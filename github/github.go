package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	name, numRepos, err := githubInfo(ctx, "tebeka")
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	fmt.Println(name, numRepos)
}

func githubInfo(ctx context.Context, login string) (string, int, error) {
	//
	url := fmt.Sprintf("https://api.github.com/users/%s", login)
	// url := "https://api.github.com/users/ + login
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return "", 0, err
	}

	resp, err := http.DefaultClient.Do(req)
	// resp, err := http.Get(url)
	if err != nil {
		return "", 0, err
	}
	if resp.StatusCode != http.StatusOK {
		return "", 0, fmt.Errorf("%#v: bad status - %s", url, resp.Status)
	}

	r := io.LimitReader(resp.Body, 1_000_000)
	return parse(r)
}

func parse(rdr io.Reader) (string, int, error) {
	var r reply // r.Name = "", r.Public_Repos = 0
	dec := json.NewDecoder(rdr)
	if err := dec.Decode(&r); err != nil {
		// log.Fatalf("error: can't decode - %s", err)
		return "", 0, err
	}
	/*
		// fmt.Println(r)
		fmt.Printf("v: %v\n", r)
		fmt.Printf("+v: %+v\n", r)
		fmt.Printf("#v: %#v\n", r) // Use %#v in logging, debug prints...
	*/
	return r.Name, r.NumRepos, nil
}

/*
type Address struct {
	Street string
	House  int
}
*/

type reply struct {
	Name string
	// Public_Repos int
	NumRepos int `json:"public_repos"` // field tag
	//Address  Address
}

/*
func get() {
	url := "https://api.github.com/users/tebeka"
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		log.Fatalf("error: %s", resp.Status)
	}
	// fmt.Println("content-type:", resp.Header.Get("Content-Type"))
	if _, err := io.Copy(os.Stdout, resp.Body); err != nil {
		log.Fatalf("error: copy - %s", err)
	}
}
*/

/* RESP API:
CRUD: Create, Retrieve, Update, Delete
Create: POST
Retrieve: GET
Update: PUT/PATCH
Delete: DELETE

JSON <-> Go type mapping
string : string
null : nil
boolean : bool
number : float64, float32, int8, int16, int32, int64, int, uint8, ...
array : []T, []any
object : struct, map[string]any

encoding/json API:
JSON -> io.Reader -> Go: Decoder
Go -> io.Writer -> JSON: Encoder
JSON -> []byte -> Go: Unmarshal
Go -> []byte -> JSON: Marshal
*/
