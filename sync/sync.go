package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

func main() {
	p := Payment{
		From:   "Wile. E. Coyote",
		To:     "ACME",
		Amount: 123.45,
	}
	p.Pay()
	p.Pay()

	// See also errgroup
	var wg sync.WaitGroup
	// wg.Add(5)
	for i := 0; i < 5; i++ {
		wg.Add(1)
		i := i
		go func() {
			defer wg.Done()
			time.Sleep(100 * time.Millisecond)
			log.Printf("%d done", i)
		}()
	}
	wg.Wait() // will block until Done matches Add
	fmt.Println("fin")
}

func (p *Payment) Pay() {
	p.once.Do(func() {
		p.pay(time.Now().UTC())
	})
}

func (p *Payment) pay(t time.Time) {
	ts := t.Format(time.RFC3339)
	fmt.Printf("[%s] %s -> $%.2f -> %s\n", ts, p.From, p.Amount, p.To)
}

type Payment struct {
	From   string
	To     string
	Amount float64 // USD

	once sync.Once
	/*
		mu   sync.Mutex
		done bool
	*/
}
